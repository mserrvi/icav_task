import pandas as pd
from flask import Flask, request, jsonify, make_response
import json

app = Flask(__name__)
app.config['SECRET_KEY'] = 'my task'

@app.route('/csv-data', methods=['POST'])
def csv_data():
    try:
        if request.get_json():
            data = request.get_json()
            if data['rows']:
                csv_row=data['rows']
                df = pd.read_csv("static/books.csv", nrows=csv_row)
                js=json.loads(df.to_json(orient='records'))
                return jsonify({'books':js})
        else:
            return make_response("rows parameter is missing",400)
    except:
        return make_response("Internal server Error",500)

@app.route('/csv-filter-data', methods=['POST'])
def csv_filter_data():
    try:
        if request.get_json():
            filter = request.get_json()
            if len(filter)==1:
                df = pd.read_csv("static/books.csv")
                for key, value in filter.items():
                    if str(key) in df.columns:
                        data = ''
                        try:
                            data = df[(df[str(key)].str.contains(str(value)))]
                        except:
                            data=df[(df[str(key).strip()] == value)]
                        d = data.to_json(orient='records')
                        return jsonify({'books': json.loads(d)})
                    else:
                        print(f"{key} not found in books")
                        return make_response("Invalid Filter", 200)
            else:
                return make_response("only one filter allowed", 200)
        else:
            return make_response("filter parameter is missing", 400)
    except:
        return make_response("Internal server Error",500)

if __name__ == "__main__":

    app.run(host='0.0.0.0')

