READMe for Monalisa Redshift Query Engine

Follow these Steps to run application in your system
1. Install docker in your system if you already have one then skip this step.

2. Now if you are logged in successfully then you can pull the private repo. for that type:
 	docker pull mserrvi/icav_task:tagname 

3. Open terminal and type command : docker run --publish 5000:5000 mserrvi/icav_task	

Api Endpoints:

1.	API will return number of rows requested from the books.csv file.
	url:- http://localhost:5000/csv-data
	Method : POST
	Headers : {
                'Content-Type': 'application/json',          
              }
    Body :    {"rows":1}
	response: {"books": [{
						"author": "Bayo Ogunjimi",
						"authors": "Bayo Ogunjimi, Abdul Rashee Na\\'allah",
						"dimensions": "8.30 (w) x 5.30 (h) x 0.80 (d)",
						"id": 1,
						"isbn10": 1592211518,
						"isbn13": 9780000000000,
						"lexile": null,
						"pages": 146,
						"price": "$23.95 ",
						"publisher": "Africa World Press",
						"pubyear": 2006,
						"subjects": "Africa - Anthropology & Sociology, African Folklore & Mythology, Oral Tradition & Storytelling, General & Miscellaneous African Literature - Literary Criticism, African Literature Anthologies, Fables, Fairy Tales, & Folk Tales - Literary Criticism",
						"title": "Introduction to African Oral Literature and Performance"
					}
				]
			}	
2. API will give freedom to the user to filter and see any data from the file. The user could only able to filter from the given column list. If a column is not present then a graceful error message should return. Even if the API didn’t find any filter response then the user should get a empty response.
	Assumptions : 
		1. "API will accept only one filter at a time."
		2. "If filter value is string then it will check in the row if filter value match with any value of whole column then it will return that row in the API response"
				example(if we send filter={"authore":'Bayo Ogunjimi'} then this filter value match with "Bayo Ogunjimi, Abdul Rashee Na\\'allah")
				
	url:- http://localhost:5000/csv-filter-data
	Method : POST 
	Headers : {
                'Content-Type': 'application/json',          
              }
    Body :    {"rows":1}
	response: {"books": [{
						"author": "Bayo Ogunjimi",
						"authors": "Bayo Ogunjimi, Abdul Rashee Na\\'allah",
						"dimensions": "8.30 (w) x 5.30 (h) x 0.80 (d)",
						"id": 1,
						"isbn10": 1592211518,
						"isbn13": 9780000000000,
						"lexile": null,
						"pages": 146,
						"price": "$23.95 ",
						"publisher": "Africa World Press",
						"pubyear": 2006,
						"subjects": "Africa - Anthropology & Sociology, African Folklore & Mythology, Oral Tradition & Storytelling, General & Miscellaneous African Literature - Literary Criticism, African Literature Anthologies, Fables, Fairy Tales, & Folk Tales - Literary Criticism",
						"title": "Introduction to African Oral Literature and Performance"
					}
				]
			}			