
import unittest
from home import app

import json
class Testing(unittest.TestCase):
    def test_csv_data_Api(self):
        tester=app.test_client(self)
        arg={"rows":2}
        response=resp = tester.post('/csv_data', data=json.dumps(arg), headers={'Content-Type': 'application/json'})
        status_code=response.status_code
        #check_status_code
        self.assertEqual(status_code,200)
        #check response data
        self.assertEqual(response.content_type,"application/json")


    def test_csv_filter_data(self):
        tester = app.test_client(self)
        arg = {"id": 1}
        response = resp = tester.post('/csv_filter_data', data=json.dumps(arg), headers={'Content-Type': 'application/json'})
        status_code = response.status_code
        # check_status_code
        self.assertEqual(status_code, 200)
        # check response data
        self.assertEqual(response.content_type, 'application/json')

if __name__ == '__main__':
    unittest.main()